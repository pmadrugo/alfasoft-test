﻿using Alfasoft_Test.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alfasoft_Test.Test.Contacts
{
    internal class TestDataSet
    {
        internal static List<Contact> DataSet = new List<Contact>()
        {
            new Contact()
            {
                ContactNumber = "000000000",
                Email = "admin@admin.com",
                IsDeleted = true, // So it is hidden
                Name = "TestContact"
            }
        };
    }
}
