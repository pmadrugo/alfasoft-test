﻿using Alfasoft_Test.Data;
using Alfasoft_Test.Data.Entities;
using Alfasoft_Test.Pages.Contacts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata;

namespace Alfasoft_Test.Test.Contacts
{
    public class Edit_Contact
    {
        private ApplicationDbContext _testContext;

        public Edit_Contact()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ContactTestDatabase")
                .Options;

            _testContext = new ApplicationDbContext(options);

            if (!_testContext.Contacts.Any(x => x.Id == 2))
            {
                _testContext.Contacts.Add(
                    new Contact()
                    {
                        Id = 2,
                        ContactNumber = "000000001",
                        Email = "admin@admins.com",
                        Name = "TestContact_2"
                    }
                );

                _testContext.SaveChanges();
            }
        }

        [Fact]

        public void Edit_Contact_EmptyName()
        {
            // Arrange
            EditModel EditPage = new EditModel(_testContext);

            // Act
            var contact = new Contact()
            {
                Id = 2,
                ContactNumber = "999999999",
                Email = "admin@admina.com"
            };

            EditPage.Contact = contact;
            EditPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    EditPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.False(isValid);
            Assert.Contains("Name", validationResults.SelectMany(x => x.MemberNames));
        }

        [Fact]
        public void Edit_Contact_Name_Too_Short()
        {
            // Arrange
            EditModel EditPage = new EditModel(_testContext);

            // Act
            var contact = new Contact()
            {
                Id = 2,
                ContactNumber = "999999999",
                Email = "admin@admina.com",
                Name = "Test"
            };

            EditPage.Contact = contact;
            EditPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    EditPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.False(isValid);
            Assert.Contains("Name", validationResults.SelectMany(x => x.MemberNames));
        }

        [Fact]
        public void Edit_Contact_MustHaveNineNumbers()
        {
            // Arrange
            EditModel EditPage = new EditModel(_testContext);

            // Act
            var contact = new Contact()
            {
                Id = 2,
                ContactNumber = "99999999",
                Email = "admin@admina.com",
                Name = "Testcontact"
            };

            EditPage.Contact = contact;
            EditPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    EditPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.False(isValid);
            Assert.Contains("ContactNumber", validationResults.SelectMany(x => x.MemberNames));
        }

        [Fact]
        public void Edit_Contact_MustHaveValidEmail()
        {
            // Arrange
            EditModel EditPage = new EditModel(_testContext);

            // Act
            var contact = new Contact()
            {
                Id = 2,
                ContactNumber = "999999999",
                Email = "test",
                Name = "Testcontact"
            };

            EditPage.Contact = contact;
            EditPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    EditPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.False(isValid);
            Assert.Contains("Email", validationResults.SelectMany(x => x.MemberNames));
        }

        [Fact]
        public async Task Edit_Contact_MustNotHaveExistingEmail()
        {
            // Arrange
            EditModel EditPage = new EditModel(_testContext);

            // Act
            var contact = new Contact()
            {
                Id = 2,
                ContactNumber = "999999999",
                Email = "admin@admin.com",
                Name = "TestContact"
            };

            EditPage.Contact = contact;
            EditPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    EditPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.True(isValid);

            try
            {
                var result = await EditPage.OnPostAsync();
                if (result is PageResult) // Has not redirected due to success
                {
                    if (!EditPage.ModelState.IsValid)
                    {
                        var error = EditPage.ModelState.GetValueOrDefault("Contact.Email");
                        Assert.NotNull(error);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [Fact]
        public async Task Edit_Contact_MustNotHaveExistingContactNumber()
        {
            // Arrange
            EditModel EditPage = new EditModel(_testContext);

            // Act
            var contact = new Contact()
            {
                Id = 2,
                ContactNumber = "000000000",
                Email = "admin@admina.com",
                Name = "TestContact"
            };

            EditPage.Contact = contact;
            EditPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    EditPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.True(isValid);

            try
            {
                var result = await EditPage.OnPostAsync();
                if (result is PageResult) // Has not redirected due to success
                {
                    if (!EditPage.ModelState.IsValid)
                    {
                        var error = EditPage.ModelState.GetValueOrDefault("Contact.ContactNumber");
                        Assert.NotNull(error);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}