using Alfasoft_Test.Data;
using Alfasoft_Test.Data.Entities;
using Alfasoft_Test.Pages.Contacts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata;

namespace Alfasoft_Test.Test.Contacts
{ 
    public class Create_Contact
    {
        private ApplicationDbContext _testContext;

        public Create_Contact()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ContactTestDatabase")
                .Options;

            _testContext = new ApplicationDbContext(options);

            if (!_testContext.Contacts.Any(x => x.Id == 1))
            {
                _testContext.Contacts.Add(
                    new Contact()
                    {
                        ContactNumber = "000000000",
                        Email = "admin@admin.com",
                        Name = "TestContact"
                    }
                );

                _testContext.SaveChanges();
            }
        }

        [Fact]
        
        public void Create_Contact_EmptyName()
        {
            // Arrange
            CreateModel createPage = new CreateModel(_testContext);

            // Act
            var contact = new Contact()
            {
                ContactNumber = "999999999",
                Email = "admin@admina.com"
            };

            createPage.Contact = contact;
            createPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    createPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.False(isValid);
            Assert.Contains("Name", validationResults.SelectMany(x => x.MemberNames));
        }

        [Fact]
        public void Create_Contact_Name_Too_Short()
        {
            // Arrange
            CreateModel createPage = new CreateModel(_testContext);

            // Act
            var contact = new Contact()
            {
                ContactNumber = "999999999",
                Email = "admin@admina.com",
                Name = "Test"
            };

            createPage.Contact = contact;
            createPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    createPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.False(isValid);
            Assert.Contains("Name", validationResults.SelectMany(x => x.MemberNames));
        }

        [Fact]
        public void Create_Contact_MustHaveNineNumbers()
        {
            // Arrange
            CreateModel createPage = new CreateModel(_testContext);

            // Act
            var contact = new Contact()
            {
                ContactNumber = "99999999",
                Email = "admin@admina.com",
                Name = "Testcontact"
            };

            createPage.Contact = contact;
            createPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    createPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.False(isValid);
            Assert.Contains("ContactNumber", validationResults.SelectMany(x => x.MemberNames));
        }

        [Fact]
        public void Create_Contact_MustHaveValidEmail()
        {
            // Arrange
            CreateModel createPage = new CreateModel(_testContext);

            // Act
            var contact = new Contact()
            {
                ContactNumber = "999999999",
                Email = "test",
                Name = "Testcontact"
            };

            createPage.Contact = contact;
            createPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    createPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.False(isValid);
            Assert.Contains("Email", validationResults.SelectMany(x => x.MemberNames));
        }

        [Fact]
        public async Task Create_Contact_MustNotHaveExistingEmail()
        {
            // Arrange
            CreateModel createPage = new CreateModel(_testContext);

            // Act
            var contact = new Contact()
            {
                ContactNumber = "999999999",
                Email = "admin@admin.com",
                Name = "TestContact"
            };

            createPage.Contact = contact;
            createPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    createPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.True(isValid);

            try
            {
                var result = await createPage.OnPostAsync();
                if (result is PageResult) // Has not redirected due to success
                {
                    if (!createPage.ModelState.IsValid)
                    {
                        var error = createPage.ModelState.GetValueOrDefault("Contact.Email");
                        Assert.NotNull(error);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [Fact]
        public async Task Create_Contact_MustNotHaveExistingContactNumber()
        {
            // Arrange
            CreateModel createPage = new CreateModel(_testContext);

            // Act
            var contact = new Contact()
            {
                ContactNumber = "000000000",
                Email = "admin@admina.com",
                Name = "TestContact"
            };

            createPage.Contact = contact;
            createPage.ModelState.Clear();

            var validationContext = new ValidationContext(contact);
            var validationResults = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(contact, validationContext, validationResults, true);

            foreach (var result in validationResults)
            {
                foreach (var name in result.MemberNames)
                {
                    createPage.ModelState.AddModelError(name, result.ErrorMessage);
                }
            }

            Assert.True(isValid);

            try
            {
                var result = await createPage.OnPostAsync();
                if (result is PageResult) // Has not redirected due to success
                {
                    if (!createPage.ModelState.IsValid)
                    {
                        var error = createPage.ModelState.GetValueOrDefault("Contact.ContactNumber");
                        Assert.NotNull(error);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}