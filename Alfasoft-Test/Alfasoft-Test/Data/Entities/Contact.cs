﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Alfasoft_Test.Data.Entities
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Name is required")]
        
        [StringLength(int.MaxValue, MinimumLength = 5, ErrorMessage = "Name must be more than 5 characters")]
        public string Name { get; set; }

        [DisplayName("Email Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Address is required")]
        [EmailAddress(ErrorMessage = "Must be valid Email Address")]
        public string Email { get; set; }

        [DisplayName("Contact")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Contact Number is required")]
        [StringLength(9, MinimumLength = 9, ErrorMessage = "Contact Number must be exactly 9 numbers")]
        [RegularExpression("[0-9]*", ErrorMessage = "Contact must be only numbers")]
        public string ContactNumber { get; set; }

        public bool IsDeleted { get; set; }
    }
}
