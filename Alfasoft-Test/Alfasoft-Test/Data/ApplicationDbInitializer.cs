﻿using Microsoft.AspNetCore.Identity;

namespace Alfasoft_Test.Data
{
    public class ApplicationDbInitializer
    {
        public static void SeedUsers(UserManager<IdentityUser> userManager)
        {
            if (userManager.FindByEmailAsync("admin@admin.com")?.Result == null)
            {
                try
                {
                    var user = new IdentityUser()
                    {
                        UserName = "admin@admin.com",
                        Email = "admin@admin.com"
                    };

                    IdentityResult result = userManager.CreateAsync(user, "ExamplePassword123!").Result;
                }
                catch (Exception ex)
                {

                    throw;
                }
                
                
            }
        }
    }
}
