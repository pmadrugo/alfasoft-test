﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Alfasoft_Test.Data;
using Alfasoft_Test.Data.Entities;
using Microsoft.AspNetCore.Authorization;

namespace Alfasoft_Test.Pages.Contacts
{
    [Authorize]
    public class EditModel : PageModel
    {
        private readonly Alfasoft_Test.Data.ApplicationDbContext _context;

        public EditModel(Alfasoft_Test.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Contact Contact { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Contacts == null)
            {
                return NotFound();
            }

            var contact =  await _context.Contacts.FirstOrDefaultAsync(m => m.Id == id);
            if (contact == null)
            {
                return NotFound();
            }
            Contact = contact;
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        { 
            var contactInDb =_context.Set<Contact>().AsNoTracking().FirstOrDefault(x => x.Id == Contact.Id);

            if (_context.Set<Contact>().Any(x => x.Email == Contact.Email) && contactInDb.Email != Contact.Email)
            {
                ModelState.AddModelError("Contact.Email", "Email is already in use");
            }

            if (_context.Set<Contact>().Any(x => x.ContactNumber == Contact.ContactNumber) && contactInDb.ContactNumber != Contact.ContactNumber)
            {
                ModelState.AddModelError("Contact.ContactNumber", "Contact Number is already in use");
            }

            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Contact).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactExists(Contact.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ContactExists(int id)
        {
          return (_context.Contacts?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
