﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Alfasoft_Test.Data;
using Alfasoft_Test.Data.Entities;
using Microsoft.AspNetCore.Authorization;

namespace Alfasoft_Test.Pages.Contacts
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly Alfasoft_Test.Data.ApplicationDbContext _context;

        public CreateModel(Alfasoft_Test.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Contact Contact { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (_context.Set<Contact>().Any(x => x.Email == Contact.Email))
            {
                ModelState.AddModelError("Contact.Email", "Email is already in use");
            }

            if (_context.Set<Contact>().Any(x => x.ContactNumber == Contact.ContactNumber))
            {
                ModelState.AddModelError("Contact.ContactNumber", "Contact Number is already in use");
            }

          if (!ModelState.IsValid || _context.Contacts == null || Contact == null)
            {
                return Page();
            }

            _context.Contacts.Add(Contact);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
