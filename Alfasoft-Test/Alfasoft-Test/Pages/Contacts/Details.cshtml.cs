﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Alfasoft_Test.Data;
using Alfasoft_Test.Data.Entities;
using Microsoft.AspNetCore.Authorization;

namespace Alfasoft_Test.Pages.Contacts
{
    [Authorize]
    public class DetailsModel : PageModel
    {
        private readonly Alfasoft_Test.Data.ApplicationDbContext _context;

        public DetailsModel(Alfasoft_Test.Data.ApplicationDbContext context)
        {
            _context = context;
        }

      public Contact Contact { get; set; } = default!; 

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Contacts == null)
            {
                return NotFound();
            }

            var contact = await _context.Contacts.FirstOrDefaultAsync(m => m.Id == id);
            if (contact == null)
            {
                return NotFound();
            }
            else 
            {
                Contact = contact;
            }
            return Page();
        }
    }
}
