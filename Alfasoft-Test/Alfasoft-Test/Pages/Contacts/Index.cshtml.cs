﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Alfasoft_Test.Data;
using Alfasoft_Test.Data.Entities;

namespace Alfasoft_Test.Pages.Contacts
{
    public class IndexModel : PageModel
    {
        private readonly Alfasoft_Test.Data.ApplicationDbContext _context;

        public IndexModel(Alfasoft_Test.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Contact> Contact { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Contacts != null)
            {
                Contact = await _context.Contacts.Where(x => !x.IsDeleted).ToListAsync();
            }
        }
    }
}
